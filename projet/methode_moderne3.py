#Version moderne

import numpy as np
import scipy.linalg as nla
import math
import matplotlib.pyplot as plt
import eu

A = eu.A
n = eu.n
m = eu.m

#Centrage et adimensionnement des données

for j in range(n):
    moy = np.mean(A[:,j])
    for i in range(m):
        A[i,j] -= moy #centrage

for j in range(n):
    ecart = math.sqrt(1/m*np.dot(A[:,j],A[:,j]))
    for i in range(m):
        A[i,j] /= ecart #divise par écart-type

def reflecteur (p, v) :
    n = v.shape[0]
    F = np.eye (n) - 2 * np.outer (v,v)
    Q = np.eye (p, dtype=np.float64)
    Q [p-n:p,p-n:p] = F
    return Q

#Construction de B

B = np.copy(A)
VL = []
VR = []
for i in range(n-2):
    x = B[i:m,i]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VL.append(v)
    Q = reflecteur (m, v)
    B = np.dot (Q, B)
    
    x = B[i,(i+1):n]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VR.append(v)
    Q = reflecteur (n, v)
    B = np.dot (B, Q)

for i in range(n-2,n):
    x = B[i:m,i]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VL.append(v)
    Q = reflecteur (m, v)
    B = np.dot (Q, B)


B = B[0:n,0:n]

#Construction de H
H = np.zeros([2*n,2*n])
H[0:n,n:2*n] = np.transpose(B)
H[n:2*n,0:n] = B

P = np.zeros([2*n,2*n])
for i in range(n):
    P[i,2*i]=1
    P[n+i,2*i+1]=1

T = np.dot(np.transpose(P),np.dot(H,P))

def compte_vp(d,e,mu):
    bi_1 = 0
    bi_2 = 0
    fi_1 = 1
    fi_2 = 0
    fi_3 = 0
    fi = 0
    ai = d[0]
    nb_val = 0
    for i in range(len(d)):
        if bi_1 == 0 and fi_1 != 0 :
            fi = (ai - mu)*np.sign(fi_1)
        elif bi_1 == 0 and fi_1 == 0 :
            fi = (ai - mu)*np.sign(fi_2)
        elif bi_2 == 0 and bi_1 != 0 and fi_2 != 0 :
            fi = (ai - mu)*fi_1 - (bi_1**2)*np.sign(fi_2)
        elif bi_2 == 0 and bi_1 != 0 and fi_2 == 0 :
            fi = (ai - mu)*fi_1 - (bi_1**2)*np.sign(fi_3)
        else :
            fi = (ai - mu)*fi_1 - (bi_1**2)*fi_2
            
        if fi_1 != 0 and fi != 0 :
            if (np.sign(fi_1) != np.sign(fi)) :
                nb_val += 1
        else :
            if fi_1 == 0 :
                if ( np.sign(fi_2) != np.sign(fi) ) :
                    nb_val += 1
            elif fi_1 == 0 and fi == 0 :
                if ( np.sign(fi_2) != np.sign(fi_1) ) :
                    nb_val += 1
        if i != 23 :
            fi_3 = fi_2
            fi_2 = fi_1
            fi_1 = fi
            bi_2 = bi_1
            bi_1 = e[i]
            ai = d[i+1]
    return nb_val
    

def valp_tri(T):
    m,n = np.shape(T)
    maxi = abs(T[0,1])
    for i in range(0,m-1):
        for j in range(1,n):
            if abs(T[i,j]) > maxi :
                maxi = abs(T[i,j])
    print("Intervalle : ",2*maxi,sep="")
    return 2*maxi

d = np.zeros (2*n)
e = np.array ([ T[i+1,i] for i in range (0,2*n-1) ])


def valp_precise(binf,bsup,ecart):
    while bsup - binf > ecart :
        mid = (bsup + binf)/2
        if compte_vp(d,e,bsup) - compte_vp(d,e,mid) == 1 :
            binf = mid
        else :
            bsup = mid
    return (bsup + binf)/2

sup = valp_tri(T)
inf = 0
inf_i = 0
lamb = []
cpt = 0
nbv_tot = compte_vp(d,e,sup) - compte_vp(d,e,inf)
while cpt != nbv_tot :
    nbv = compte_vp(d,e,sup) - compte_vp(d,e,inf)
    while nbv != 1 :
        if nbv > 1 :
            inf_i = inf
            inf = (sup + inf)/2
        if nbv == 0 :
            sup = inf
            inf = (inf + inf_i)/2
        nbv = compte_vp(d,e,sup) - compte_vp(d,e,inf)
    lamb.append(valp_precise(inf,sup,0.00001))
    sup = inf
    inf = 0
    cpt += 1

lamb.sort()
lamb = np.array(lamb)

n,m = np.shape(T)
cpt = 0
vec = []
while cpt != 12 :
    P1 = T - lamb[cpt]*np.eye(n)
    P2,L,U = nla.lu(P1)
    v2=[]
    v1 = np.array([1 for i in range(n)])
    v1 = (1/nla.norm(v1,2))*v1
    for i in range(100) :
        w = nla.solve_triangular(L,v1,lower=True)
        w = nla.solve_triangular(U, w, lower=False)
        v1 = (1/nla.norm(w,2))*w
    v2 = [v1[i] for i in range(len(v1))]
    vec.append(v2)
    cpt += 1

vec = np.array(vec)
vec = np.transpose(vec)

n = eu.n
m = eu.m

Lambda = lamb
Q = vec

Y = math.sqrt(2)*np.dot(P,Q)

newVt = np.transpose (Y[0:n,:])
newU = np.zeros ([m,n], dtype=np.float64)
newU[0:n,:] = Y[n:2*n,:]

newSigma = np.array (np.diag (Lambda))

for i in range(n-1,-1,-1):
    Q = reflecteur(m,VL[i])
    newU = np.dot(Q,newU)

for i in range (n-3, -1, -1) :
    Q = reflecteur (n, VR[i])
    newVt = np.dot (newVt, Q)
    
qi = newVt[-1,0:n]
qj = newVt[-2,0:n]

B = np.transpose(np.array([qi,qj]))
P = np.dot(A,B)

for i in range(m):
    print(P[i][0],P[i][1],eu.lignes[i])
    
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (eu.lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()