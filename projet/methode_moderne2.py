#Version moderne

import numpy as np
import scipy.linalg as nla
import math
import matplotlib.pyplot as plt
import eu

A = eu.A
n = eu.n
m = eu.m

#Centrage et adimensionnement des données

for j in range(n):
    moy = np.mean(A[:,j])
    for i in range(m):
        A[i,j] -= moy #centrage

for j in range(n):
    ecart = math.sqrt(1/m*np.dot(A[:,j],A[:,j]))
    for i in range(m):
        A[i,j] /= ecart #divise par écart-type

def reflecteur (p, v) :
    n = v.shape[0]
    F = np.eye (n) - 2 * np.outer (v,v)
    Q = np.eye (p, dtype=np.float64)
    Q [p-n:p,p-n:p] = F
    return Q

#Construction de B

B = np.copy(A)
VL = []
VR = []
for i in range(n-2):
    x = B[i:m,i]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VL.append(v)
    Q = reflecteur (m, v)
    B = np.dot (Q, B)
    
    x = B[i,(i+1):n]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VR.append(v)
    Q = reflecteur (n, v)
    B = np.dot (B, Q)

for i in range(n-2,n):
    x = B[i:m,i]
    v = np.copy(x)
    v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
    v = (1/nla.norm(v,2))*v
    VL.append(v)
    Q = reflecteur (m, v)
    B = np.dot (Q, B)


B = B[0:n,0:n]

#Construction de H
H = np.zeros([2*n,2*n])
H[0:n,n:2*n] = np.transpose(B)
H[n:2*n,0:n] = B

P = np.zeros([2*n,2*n])
for i in range(n):
    P[i,2*i]=1
    P[n+i,2*i+1]=1

T = np.dot(np.transpose(P),np.dot(H,P))

d = np.zeros (2*n)
e = np.array ([ T[i+1,i] for i in range (0,2*n-1) ])

eigvals, eigvecs = nla.eigh_tridiagonal(d, e)
Lambda = eigvals [n:2*n]
Q = eigvecs [:,n:2*n]

Y = math.sqrt(2)*np.dot(P,Q)

newVt = np.transpose (Y[0:n,:])
newU = np.zeros ([m,n], dtype=np.float64)
newU[0:n,:] = Y[n:2*n,:]

newSigma = np.array (np.diag (Lambda))

for i in range(n-1,-1,-1):
    Q = reflecteur(m,VL[i])
    newU = np.dot(Q,newU)

for i in range (n-3, -1, -1) :
    Q = reflecteur (n, VR[i])
    newVt = np.dot (newVt, Q)
    
qi = newVt[-1,0:n]
qj = newVt[-2,0:n]

B = np.transpose(np.array([qi,qj]))
P = np.dot(A,B)

for i in range(m):
    print(P[i][0],P[i][1],eu.lignes[i])
    
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (eu.lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()
