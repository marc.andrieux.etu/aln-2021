#Version moderne

import numpy as np
import scipy.linalg as nla
import math
import matplotlib.pyplot as plt
import eu

A = eu.A
n = eu.n
m = eu.m

#Centrage et adimensionnement des données

for j in range(n):
    moy = np.mean(A[:,j])
    for i in range(m):
        A[i,j] -= moy #centrage

for j in range(n):
    ecart = math.sqrt(1/m*np.dot(A[:,j],A[:,j]))
    for i in range(m):
        A[i,j] /= ecart #divise par écart-type

u, s, v = nla.svd(A, full_matrices=False)

#indice 0 et 1 pour les deux plus grandes valeurs propres

#On prend la première ligne car la décomposition nous donne v transposée
v1 = v[0,:] 
v2 = v[1,:]

B = np.transpose(np.array([v1,v2]))
P = np.dot(A,B)

for i in range(m):
    print(P[i][0],P[i][1],eu.lignes[i])
    
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (eu.lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()