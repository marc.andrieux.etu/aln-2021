#Version historique sans nla.eigh

import numpy as np
import scipy.linalg as nla
import math
import matplotlib.pyplot as plt
import eu

A = eu.A
n = eu.n
m = eu.m

#Centrage et adimensionnement des données

for j in range(n):
    moy = np.mean(A[:,j])
    for i in range(m):
        A[i,j] -= moy #centrage

for j in range(n):
    ecart = math.sqrt(1/m*np.dot(A[:,j],A[:,j]))
    for i in range(m):
        A[i,j] /= ecart #divise par écart-type
    
C = (1/(n-1))*np.dot(np.transpose(A),A)

#algo qr pour trouver une matrice triangulaire supérieur semblable à C ainsi que la matrice de passage
#Inspiré par la proposition 22 p98 du cours
def algoQR(A):
    Ai = A
    Q = np.eye(n)
    for i in range(100):
        Qi, Ri = nla.qr(Ai)
        Ai = np.dot(Ri,Qi)
        Q = np.dot(Q,Qi)
    return Ai,Q

#Ai converge vers une matrice triangulaire supérieure semblable à C

T, Q = algoQR(C)

#Valeurs propres triées par ordre décroissant donc indoce 0 et 1 pour les deux plus grandes
qi = Q[:,0]
qj = Q[:,1]

B = np.transpose(np.array([qi,qj]))
P = np.dot(A,B)

for i in range(m):
    print(P[i][0],P[i][1],eu.lignes[i])
    
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (eu.lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()