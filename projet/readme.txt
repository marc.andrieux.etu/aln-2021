Dans ce depot git vous pourrez trouver notre rendu au format pdf.
Vous pourrez également trouver nos differentes versions du projet.

Les versions sont les suivantes :
methode_covariance.py  -> on utilise la méthode historique avec la fonction nla.eigh
methode_covariance2.py -> on utilise la méthode historique avec un algorithme QR
methode_moderne.py -> on utilise la méthode moderne avec la fonction nla.svd
methode_moderne2.py -> on code nous même la fonction nla.svd
methode_moderne3.py -> on code nous même la fonction nla.eigh_tridiagonal
