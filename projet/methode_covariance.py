#Version historique

import numpy as np
import scipy.linalg as nla
import math
import matplotlib.pyplot as plt
import eu

A = eu.A
n = eu.n
m = eu.m

#Centrage et adimensionnement des données

for j in range(n):
    moy = np.mean(A[:,j])
    for i in range(m):
        A[i,j] -= moy #centrage

for j in range(n):
    ecart = math.sqrt(1/m*np.dot(A[:,j],A[:,j]))
    for i in range(m):
        A[i,j] /= ecart #divise par écart-type
    
C = (1/(n-1))*np.dot(np.transpose(A),A)

l, v = nla.eigh(C)
        
qi = v[:,len(l)-1]
qj = v[:,len(l)-2]

#Coordonnées des points

B = np.transpose(np.array([qi,qj]))
P = np.dot(A,B)

for i in range(m):
    print(P[i][0],P[i][1],eu.lignes[i])
    
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (eu.lignes[i], P[i,:])

plt.scatter (0, 0, color='white')

plt.show ()