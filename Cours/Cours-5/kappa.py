import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt
from scipy.stats import ortho_group

abscissas = []
ordinates = []
# La dimension du système A . x = b
m = 10

# Pour kappa variant de 10**5, 10**6, ... à 10**15
for exponent in range(5,15) :
# On calcule deux matrices orthogonales aléatoires U et Vt
    U = ortho_group.rvs(m)
    Vt = ortho_group.rvs(m)
# On calcule une matrice diagonale en utilisant kappa
    kappa = 10**exponent
    Sigma = np.diag ([kappa**(-i/(m-1)) for i in range(m)])
# On calcule A = U . Sigma . Vt
# Note : on a calculé A à partir de sa SVD (singular value
#   decomposition). Les éléments de Sigma sont les valeurs
#   singulières de A. Quand kappa est grand, certaines sont 
#   proches de zéro et A est proche d'une matrice singulière
    A = np.dot (U, np.dot (Sigma, Vt))
# Estimation de kappa
    est_kappa = np.linalg.cond(A)
# On triche : connaissant x, on calcule b et on essaie de
#   retrouver x (dans xback)
    x = np.random.rand(m)
    b = np.dot (A,x)
    xback = nla.solve (A,b)
# L'erreur relative
    errrel = np.linalg.norm(x-xback)/np.linalg.norm(x)
    abscissas.append(exponent)
    ordinates.append(- math.log(errrel)/math.log(10))

# Calcul des coefficients de la droite qui approxime
#   le mieux les points au sens des moindres carrés
a = np.polyfit(abscissas, ordinates, 1)
def f(x) :
    return a[0]*x+a[1]

# Graphique
plt.figure ()
plt.axes ()
plt.scatter (abscissas, ordinates, color='orange', label='erreurs expérimentales')
x = np.linspace(abscissas[0], abscissas[-1], 20)
plt.plot (x, f(x), label=str(a[0]) + '*x + ' + str(a[1]))
plt.legend ()
plt.show ()

