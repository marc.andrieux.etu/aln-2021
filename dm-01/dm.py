#Andrieux Marc et Leblanc Rémy

import numpy as np
import scipy.linalg as nla

#1/
print("Question 1 :")
def f(x):
    return x**2 -3 #sqrt(3) est solution

def df(x):
    return 2*x #dérivée de f

u = []
u.append(1)
print("u0 =",u[0])
for i in range(1,15):
    u.append(u[i-1]-f(u[i-1])/df(u[i-1]))
    print("u",i," = ",u[i],sep="")

print("On retrouve bien la valeur de sqrt(3)")

#2/
print("\nQuestion 2 :")
def f(x,y):
    return np.array([x**2 + 2*x*y -1,x**2 * y**2 - y -3],dtype=np.float64)

print("On test que (2,-3/4) est solution du système : f(2,-3/4) =",f(2,-3/4))

def J(x,y): #Jacobienne
    return np.array([[2*x+2*y,2*x],[2*x*(y**2),2*(x**2)*y-1]],dtype=np.float64)

u = []
u.append([3,-1])

for i in range(6):
    Jn = J(u[i][0],u[i][1])
    fn = -f(u[i][0],u[i][1])
    Q,R = nla.qr(Jn) #On utilise la factorisation QR
    z = nla.solve_triangular(R,np.dot(np.transpose(Q),fn),lower=False)
    u.append([u[i][0]+z[0],u[i][1]+z[1]])

for i in range(6):
    print("u",i,"=",u[i],sep="")
