import numpy as np
import scipy.linalg as nla

np.set_printoptions(linewidth=240)

def reflecteur (m, v) :
    n = v.shape[0]
    F = np.eye(n,dtype=np.float64) - (2/np.dot(v,v))*np.outer(v,v)
    Q = np.eye(m,dtype=np.float64)
    Q[m-n:m,m-n:m] = F
    return Q

A = np.array([[1,7,-3,4],[2,0,-8,1],[1,1,-1,-1],[2,1,3,5]], dtype=np.float64)
x = A[2:m,2]
v = np.copy(x)
v[0] = v[0]+np.sign(v[0])*nla.norm(x,2)

x = A[1:m,0]
v = np.copy(x)
v[0] = v[0]+np.sign(v[0])*nla.norm(x,2)
Q1 = reflecteur (m, v)
A2 = np.dot (np.dot (Q1, A), np.transpose(Q1))

x = A2[2:m,1]
v = np.copy(x)
v[0] = v[0]+np.sign(v[0])*nla.norm(x,2)
Q2 = reflecteur (m, v)
A3 = np.dot (np.dot (Q2, A2), np.transpose(Q2))

# Retourne Q et H tels que Q**T . H . Q = A

def Hessenberg (A) :
    m = A.shape[0]
    B = A
    Q = np.eye (m)
    for i in range (1,m-1) :
        x = B[i:m,i-1]
        v = np.copy (x)
        v[0] = v[0]+np.sign(v[0])*nla.norm(x,2)
        Qi = reflecteur (m, v)
        Q = np.dot (Qi, Q)
        B = np.dot (np.dot (Qi, B), np.transpose (Qi))
    return B, Q



