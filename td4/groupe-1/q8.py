import numpy as np
import scipy.linalg as nla
M = np.array ([[1,-1,4],[1,4,-2],[1,4,2],[1,-1,0]], dtype=np.float64)
QM, RM = nla.qr(M)
Lambda = np.diag ([128, -64, 32, 16]) ; Lambda
A = np.dot (QM, np.dot (Lambda, np.transpose(QM))) ; A

np.set_printoptions(linewidth=240)

def reflecteur (m, v) :
    n = v.shape[0]
    F = np.eye (n) - (2 / np.dot (v,v)) * np.outer (v, v)
    Q = np.eye (m, dtype=np.float64)
    Q[m-n:m,m-n:m] = F
    return Q

x = A[:,0]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
Q = reflecteur(4, v)
np.dot (Q, A)


# Mise sous forme de Hessenberg de A

m = 4
x = A[1:m,0]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
Q1 = reflecteur (m, v)
A2 = np.dot (np.dot (Q1, A), np.transpose(Q1))
x = A2[2:m,1]
v = np.copy(x)
v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
Q2 = reflecteur (m, v)
A3 = np.dot (np.dot (Q2, A2), np.transpose(Q2))




